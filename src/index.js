function MyArray(...args) {
  this._length = args.length;

  if (args.length === 1 && args[0] < 0) {
    throw new RangeError('Invalid array length');
  }

  if (args.length === 1 && args[0] > (2 ** 32 - 1)) {
    throw new RangeError('Invalid array length');
  }

  if (args.length === 1 && typeof args[0] === 'number' && isNaN(args[0])) {
    throw new RangeError('Invalid array length');
  }

  if (args.length === 1 && typeof args[0] === 'number') {
    this._length = args[0];
  } else {
    for (let i = 0; i < args.length; i++) {
      this[i] = args[i];
    }
  }
  Object.defineProperty(this, '_length', {
    enumerable: false,
    writable: true
  });
  Object.defineProperty(this, 'length', {
    enumerable: false,
    set(lengthToSet) {
      if (typeof lengthToSet !== 'number') {
        throw new RangeError('Invalid array length');
      }

      if (lengthToSet < this.length) {
        for (let i = lengthToSet; i < this.length; i++) {
          delete this[i];
        }
      }
      this._length = lengthToSet;
    },
    get() {
      return this._length;
    }
  });
}

Object.defineProperty(MyArray, 'length', { value: 1 });


MyArray.prototype.pop = function() {
  const last = this[this.length - 1];
  delete this[this.length - 1];
  this.length = this.length - 1;
  return last;
};

MyArray.prototype.push = function(...args) {
  const isMyArrayInstance = this instanceof MyArray;

  if (this.length !== undefined && typeof this.length !== 'number') {
    this.length = 0;
  }

  if (!isMyArrayInstance) {
    if (this.length === undefined) {
      this.length = 0;
      this._length = 0;
    } else if (typeof this.length === 'number') {
      this._length = this.length;
      this.length = this.length + args.length;
    }
  }

  for (let i = 0; i < args.length; i++) {
    this[this._length + i] = args[i];
  }
  this._length = this._length + args.length;

  if (!isMyArrayInstance) {
    delete this._length;
  }

  return this.length;
};

Object.defineProperty(MyArray.prototype.push, 'length', { value: 1, enumerable: false });

MyArray.from = function(arrayLike, mapFn) {
  if (!arrayLike) {
    throw new Error('\'Array.from requires an array-like object - not null or undefined\'');
  }

  if (mapFn && !mapFn.call) {
    throw new Error('Array.from: when provided, the second argument must be a function');
  }

  const instance = new MyArray();

  for (let i = 0; i < arrayLike.length; i++) {
    if (!mapFn) {
      instance.push(arrayLike[i]);
    } else {
      const modified = mapFn(arrayLike[i]);
      instance.push(modified);
    }
  }
  return instance;
};

MyArray.prototype.map = function(callback) {
  if (!callback || !callback.call) {
    throw new Error(`${callback} is not a function`);
  }

  const newInstance = new MyArray();

  for (let i = 0; i < this.length; i++) {
    const callbackResult = callback(this[i], i, this);
    newInstance.push(callbackResult);
  }
  return newInstance;
};


MyArray.prototype.forEach = function(callback, thisArg) {
  const cbFn = callback.bind(thisArg);
  const newLength = this.length;

  for (let i = 0; i < newLength; i++) {
    if (i in this) {
      cbFn(this[i], i, this);
    }
  }
};
Object.defineProperty(MyArray.prototype.forEach, 'length', { value: 1, enumerable: false });

MyArray.prototype.reduce = function(callback) {
  if (!callback || !callback.call) {
    throw new Error(`${callback} is not a function`);
  }

  const initial = arguments[1];
  let accumulator = arguments.length > 1 ? initial : this[0];
  const newLength = this.length;
  const beginIndex = arguments.length > 1 ? 0 : 1;

  if (!newLength && !initial) {
    throw new TypeError();
  }

  for (let i = beginIndex; i < newLength; i++) {
    if (i in this) {
      accumulator = callback(accumulator, this[i], i, this);
    }
  }
  return accumulator;
};
Object.defineProperty(MyArray.prototype.reduce, 'length', { value: 1, enumerable: false });

MyArray.prototype.filter = function(callback, thisArg) {
  if (!callback || !callback.call) {
    throw new TypeError(`${callback} is not a function`);
  }

  if (this === null) {
    throw new TypeError('Array.prototype.reduce called on null or undefined');
  }

  const newArr = new MyArray();
  const cbFn = callback.bind(thisArg);
  const newLength = this.length;

  for (let i = 0; i < newLength; i++) {
    if (i in this) {
      const callbackResult = cbFn(this[i], i, this);

      if (callbackResult === true) {
        newArr.push(this[i]);
      }
    }
    this.length += 1;
  }

  return newArr;
};
Object.defineProperty(MyArray.prototype.filter, 'length', { value: 1, enumerable: false });

MyArray.prototype.sort = function(compareFunction) {
  for (let j = 0, endJ = this.length - 1; j < endJ; j++) {
    for (let i = 0, endI = endJ - j; i < endI; i++) {
      if (!compareFunction) {
        if (toString(this[i]) > toString(this[i + 1])) {
          const k = this[i];
          this[i] = this[i + 1];
          this[i + 1] = k;
        }
      } else {
        const compareResult = compareFunction(this[i], this[i + 1]);

        if (compareResult > 0) {
          const k = this[i];
          this[i] = this[i + 1];
          this[i + 1] = k;
        }
      }
    }
  }
  return this;
};

function compareNumbers(a, b) {
  return b - a;
}

MyArray.prototype.toString = function() {
  let newString = String(this[0]);

  for (let i = 1; i < this.length; i++) {
    newString += `,${String(this[i])}`;
  }
  return newString;
};

MyArray.prototype[Symbol.iterator] = function() {
  let key = -1;
  const arr = this;

  return {
    next() {
      key += 1;
      return {
        value: arr[key],
        done: !(key in arr)
      };
    }
  };
};

module.exports = MyArray;
