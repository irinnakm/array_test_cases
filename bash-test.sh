#!/bin/bash

date > index.html
{
  docker run -i --name irina -v `pwd`:/usr/src/app -w /usr/src/app node:alpine sh -c 'npm install && npm run lint && npm test' &&
  echo '<pre>' >> index.html &&
  docker run --rm grycap/cowsay /usr/games/cowsay "Everything is ok!" >> index.html &&
  echo '<pre>' >> index.html 
} || {
  echo '<pre>' >> index.html &&
   docker logs irina 2>&1 | tee -ai index.html
   echo '<pre>' >> index.html 
 }
 docker rm irina
echo 'FROM nginx:latest' > Dockerfile
echo 'COPY ./index.html /usr/share/nginx/html/index.html' >> Dockerfile

docker login registry.gitlab.com -u "irinakm" -p "25800irinna"
docker pull registry.gitlab.com/irinnakm/array_test_cases
docker tag registry.gitlab.com/irinnakm/array_test_cases registry.gitlab.com/irinnakm/array_test_cases:backup
docker push registry.gitlab.com/irinnakm/array_test_cases:backup
docker rmi registry.gitlab.com/irinnakm/array_test_cases:backup
docker build -t registry.gitlab.com/irinnakm/array_test_cases .
docker push registry.gitlab.com/irinnakm/array_test_cases